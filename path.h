/*
**  OSSP path - Filesystem Path Manipulation
**  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland <http://www.cw.com/de/>
**
**  This file is part of OSSP path, a filesystem path manipulation library
**  which can be found at http://www.ossp.org/pkg/lib/path/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  path.h: API definition
*/

#ifndef __PATH_H__
#define __PATH_H__

typedef enum {
    PATH_OK = 0,
    PATH_ERR_ARG,
    PATH_ERR_USE,
    PATH_ERR_INT,
    PATH_ERR_SYS,
    PATH_ERR_MEM,
    PATH_ERR_EXS,
    PATH_ERR_NFD
} path_rc_t;

typedef enum {
    PATH_TEMP_FILE,
    PATH_TEMP_DIR
} path_temp_t;

char *path_abs2rel  (char *, size_t, const char *, const char *);
char *path_rel2abs  (char *, size_t, const char *, const char *);
char *path_resolve  (char *, size_t, const char *);
char *path_dirname  (char *, size_t, const char *);
char *path_basename (char *, size_t, const char *);
path_rc_t path_canon(char *res_buf, size_t res_len, const char *path_buf, size_t path_len);
path_rc_t path_temp(path_temp_t id, const char *tmpl, char **res_ptr, size_t *res_size, int *res_fd);
path_rc_t path_self (char *res_buf, size_t res_size, const char *argv0);

#endif /* __PATH_H__ */

