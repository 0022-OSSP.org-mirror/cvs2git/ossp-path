/*
**  OSSP path - Filesystem Path Manipulation
**  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland <http://www.cw.com/de/>
**
**  This file is part of OSSP path, a filesystem path manipulation library
**  which can be found at http://www.ossp.org/pkg/lib/path/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  path_test.c: test suite
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/param.h>

#include "path.h"

static void usage(const char *argv0, const char *error)
{
    if (error != NULL)
        fprintf(stderr, "error: %s\n", error);
    fprintf(stderr, "usage: %s abs2rel <path> [<base>]\n", argv0);
    fprintf(stderr, "usage: %s rel2abs <path> [<base>]\n", argv0);
    fprintf(stderr, "usage: %s resolve <path>\n", argv0);
    fprintf(stderr, "usage: %s dirname <path>\n", argv0);
    fprintf(stderr, "usage: %s basename <path>\n", argv0);
    fprintf(stderr, "usage: %s temp dir|file [<template>]\n", argv0);
    fprintf(stderr, "usage: %s self\n", argv0);
    if (error != NULL)
        exit(1);
    else
        exit(0);
}

int main(int argc, char *argv[])
{
    char res[MAXPATHLEN];
    char cwd[MAXPATHLEN];
    char *rv;

    if ((argc >= 3 && argc <= 4) && strcmp(argv[1], "abs2rel") == 0) {
        if (argc == 3) {
            if (getcwd(cwd, MAXPATHLEN) == NULL) {
                fprintf(stderr, "error: cannot get current directory\n");
                exit(1);
            }
        } else
            strcpy(cwd, argv[3]);
        rv = path_abs2rel(res, sizeof(res), argv[2], cwd);
        if (rv != NULL)
            printf("%s\n", res);
        else
            printf("ERROR\n");
    }
    else if ((argc >= 3 && argc <= 4) && strcmp(argv[1], "rel2abs") == 0) {
        if (argc == 3) {
            if (getcwd(cwd, MAXPATHLEN) == NULL) {
                fprintf(stderr, "error: cannot get current directory\n");
                exit(1);
            }
        } else
            strcpy(cwd, argv[3]);
        rv = path_rel2abs(res, sizeof(res), argv[2], cwd);
        if (rv != NULL)
            printf("%s\n", res);
        else
            printf("ERROR\n");
    }
    else if (argc == 3 && strcmp(argv[1], "resolve") == 0) {
        rv = path_resolve(res, sizeof(res), argv[2]);
        if (rv != NULL)
            printf("%s\n", res);
        else
            printf("ERROR\n");
    }
    else if (argc == 3 && strcmp(argv[1], "canon") == 0) {
        if (path_canon(res, sizeof(res), argv[2], 0) == PATH_OK)
            printf("%s\n", res);
        else
            printf("ERROR\n");
    }
    else if (argc == 3 && strcmp(argv[1], "dirname") == 0) {
        rv = path_dirname(res, sizeof(res), argv[2]);
        if (rv != NULL)
            printf("%s\n", res);
        else
            printf("ERROR\n");
    }
    else if (argc == 3 && strcmp(argv[1], "basename") == 0) {
        rv = path_basename(res, sizeof(res), argv[2]);
        if (rv != NULL)
            printf("%s\n", res);
        else
            printf("ERROR\n");
    }
    else if ((argc >= 3 && argc <= 4) && strcmp(argv[1], "temp") == 0) {
        path_temp_t id = 0;
        char *tmpl;
        char *result;
        size_t result_len;
        if (strcmp(argv[2], "dir") == 0)
            id = PATH_TEMP_DIR;
        else if (strcmp(argv[2], "file") == 0)
            id = PATH_TEMP_FILE;
        else
            usage(argv[0], "invalid temp id");
        tmpl = NULL;
        if (argc == 4)
            tmpl = argv[3];
        result = res;
        if (path_temp(id, tmpl, &result, &result_len, NULL) != PATH_OK)
            fprintf(stderr, "ERROR\n");
        else {
            fprintf(stderr, "%s\n", res);
            unlink(res);
        }
    }
    else if (argc == 2 && strcmp(argv[1], "self") == 0) {
        if (path_self(res, sizeof(res), argv[0]) != PATH_OK)
            fprintf(stderr, "ERROR\n");
        else
            fprintf(stderr, "%s\n", res);
    }
    else
        usage(argv[0], "invalid usage");
    exit(0);
}

