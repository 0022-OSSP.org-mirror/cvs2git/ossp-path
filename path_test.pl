
@abs2rel = (
    'a/b/c          /               a/b/c',
    'a/b/c          /a              a/b/c',
    '/a/b/c         a               ERROR',
);

@rel2abs = (
    '/a/b/c         /               /a/b/c',
    '/a/b/c         /a              /a/b/c',
    'a/b/c          a               ERROR',
    '..             /a              /',
    '../            /a              /',
    '../..          /a              /',
    '../../         /a              /',
    '../../..       /a              /',
    '../../../      /a              /',
    '../b           /a              /b',
    '../b/          /a              /b/',
    '../../b        /a              /b',
    '../../b/       /a              /b/',
    '../../../b     /a              /b',
    '../../../b/    /a              /b/',
    '../b/c         /a              /b/c',
    '../b/c/        /a              /b/c/',
    '../../b/c      /a              /b/c',
    '../../b/c/     /a              /b/c/',
    '../../../b/c   /a              /b/c',
    '../../../b/c/  /a              /b/c/',
);

@common = (
    '/a/b/c         /a/b/c          .',
    '/a/b/c         /a/b/           c',
    '/a/b/c         /a/b            c',
    '/a/b/c         /a/             b/c',
    '/a/b/c         /a              b/c',
    '/a/b/c         /               a/b/c',
    '/a/b/c         /a/b/c          .',
    '/a/b/c         /a/b/c/         .',
    '/a/b/c/        /a/b/c          ./',
    '/a/b/          /a/b/c          ../',
    '/a/b           /a/b/c          ..',
    '/a/            /a/b/c          ../../',
    '/a             /a/b/c          ../..',
    '/              /a/b/c          ../../../',
    '/a/b/c         /a/b/z          ../c',
    '/a/b/c         /a/y/z          ../../b/c',
    '/a/b/c         /x/y/z          ../../../a/b/c',
);

@canon = (
    'a              a',
    'a/             a',
    'a/b            a/b',
    'a////b         a/b',
    '.              .',
    'a/..           .',
    'a/../../..     ../..',
    '/a/../../../b  /b',
);

$cnt = 0;
$progname = './path_test';

foreach (@abs2rel) {
    @d = split;
    chop($result = `./$progname abs2rel $d[0] $d[1]`);
    if ($d[2] eq $result) {
        print STDERR "OK: abs2rel: $d[0] $d[1] -> $result\n";
    } else {
        print STDERR "ERROR: abs2rel: $d[0] $d[1] -> $result (It should be '$d[2]')\n";
        $cnt++;
    }
}
foreach (@common) {
    @d = split;
    chop($result = `./$progname abs2rel $d[0] $d[1]`);
    if ($d[2] eq $result) {
        print STDERR "OK: abs2rel: $d[0] $d[1] -> $result\n";
    } else {
        print STDERR "ERROR: abs2rel: $d[0] $d[1] -> $result (It should be '$d[2]')\n";
        $cnt++;
    }
}

foreach (@rel2abs) {
    @d = split;
    chop($result = `./$progname rel2abs $d[0] $d[1]`);
    if ($d[2] eq $result) {
        print STDERR "OK: rel2abs: $d[0] $d[1] -> $result\n";
    } else {
        print STDERR "ERROR: rel2abs: $d[0] $d[1] -> $result (It should be '$d[2]')\n";
        $cnt++;
    }
}

foreach (@common) {
    @d = split;
    chop($result = `./$progname rel2abs $d[2] $d[1]`);
    if ($d[0] eq $result) {
        print STDERR "OK: rel2abs: $d[0] $d[1] -> $result\n";
    } else {
        print STDERR "ERROR: rel2abs: $d[0] $d[1] -> $result (It should be '$d[2]')\n";
        $cnt++;
    }
}

foreach (@canon) {
    @d = split;
    chop($result = `./$progname canon $d[0]`);
    if ($d[1] eq $result) {
        print STDERR "OK: canon: $d[0] -> $result\n";
    } else {
        print STDERR "ERROR: canon: $d[0] -> $result (It should be '$d[1]')\n";
        $cnt++;
    }
}

close(LOG);

if ($cnt == 0) {
    print STDERR "Total: OK.\n";
} else {
    print STDERR "Total: $cnt errors detected.\n";
}

