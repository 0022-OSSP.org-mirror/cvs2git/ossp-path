/*
**  OSSP path - Filesystem Path Manipulation
**  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland <http://www.cw.com/de/>
**
**  This file is part of OSSP path, a filesystem path manipulation library
**  which can be found at http://www.ossp.org/pkg/lib/path/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  path_abs2rel.c: absolute to relative path translation
*/

/*
 * Copyright (c) 1997 Shigio Yamaguchi. All rights reserved.
 * Copyright (c) 1999 Tama Communications Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by Tama Communications
 *      Corporation.
 * 4. Neither the name of the author nor the names of any co-contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "path.h"

/*
 * abs2rel: convert an absolute path name into relative.
 */
char *
path_abs2rel(
    char *result,
    size_t size,
    const char *path,
    const char *base)
{
    const char *pp, *bp, *branch;
    const char *endp;
    char *rp;

    /* endp points the last position which is safe in the result buffer. */
    endp = result + size - 1;

    if (*path != '/') {
        if (strlen(path) >= size)
            goto erange;
        strcpy(result, path);
        goto finish;
    }
    else if (*base != '/' || !size) {
        errno = EINVAL;
        return NULL;
    }
    else if (size == 1) {
        goto erange;
    }

    /*
     * seek to branched point.
     */
    branch = path;
    for (pp = path, bp = base; *pp && *bp && *pp == *bp; pp++, bp++)
        if (*pp == '/')
            branch = pp;
    if ((*pp == 0 || (*pp == '/' && *(pp + 1) == 0)) &&
        (*bp == 0 || (*bp == '/' && *(bp + 1) == 0))) {
        rp = result;
        *rp++ = '.';
        if (*pp == '/' || *(pp - 1) == '/')
            *rp++ = '/';
        if (rp > endp)
            goto erange;
        *rp = 0;
        goto finish;
    }
    if ((*pp == 0 && *bp == '/') || (*pp == '/' && *bp == 0))
        branch = pp;

    /*
     * up to root.
     */
    rp = result;
    for (bp = base + (branch - path); *bp; bp++) {
        if (*bp == '/' && *(bp + 1) != 0) {
            if (rp + 3 > endp)
                goto erange;
            *rp++ = '.';
            *rp++ = '.';
            *rp++ = '/';
        }
    }
    if (rp > endp)
        goto erange;
    *rp = 0;

    /*
     * down to leaf.
     */
    if (*branch) {
        if (rp + strlen(branch + 1) > endp)
            goto erange;
        strcpy(rp, branch + 1);
    }
    else {
        *--rp = 0;
    }

finish:
    return result;

erange:
    errno = ERANGE;
    return NULL;
}

