/*
**  OSSP path - Filesystem Path Manipulation
**  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland <http://www.cw.com/de/>
**
**  This file is part of OSSP path, a filesystem path manipulation library
**  which can be found at http://www.ossp.org/pkg/lib/path/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  path_rel2abs.c: relative to absolute path translation
*/

/*
 * Copyright (c) 1997 Shigio Yamaguchi. All rights reserved.
 * Copyright (c) 1999 Tama Communications Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by Tama Communications
 *      Corporation.
 * 4. Neither the name of the author nor the names of any co-contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "path.h"

/*
 * rel2abs: convert an relative path name into absolute.
 */
char *
path_rel2abs(
    char *result,
    size_t size,
    const char *path,
    const char *base)
{
    const char *pp, *bp;
    const char *endp;
    char *rp;
    int length;

    /* endp points the last position which is safe in the result buffer. */
    endp = result + size - 1;

    if (*path == '/') {
        if (strlen(path) >= size)
            goto erange;
        strcpy(result, path);
        goto finish;
    }
    else if (*base != '/' || !size) {
        errno = EINVAL;
        return (NULL);
    }
    else if (size == 1) {
        goto erange;
    }

    if (!strcmp(path, ".") || !strcmp(path, "./")) {
        if (strlen(base) >= size)
            goto erange;
        strcpy(result, base);

        /* rp points the last char. */
        rp = result + strlen(base) - 1;
        if (*rp == '/')
            *rp = 0;
        else
            rp++;

        /* rp point NULL char */
        if (*++path == '/') {
            /* Append '/' to the tail of path name. */
            *rp++ = '/';
            if (rp > endp)
                goto erange;
            *rp = 0;
        }
        goto finish;
    }
    bp = base + strlen(base);
    if (*(bp - 1) == '/')
        --bp;

    /*
     * up to root.
     */
    for (pp = path; *pp && *pp == '.'; ) {
        if (!strncmp(pp, "../", 3)) {
            pp += 3;
            while (bp > base && *--bp != '/')
                ;
        }
        else if (!strncmp(pp, "./", 2)) {
            pp += 2;
        }
        else if (!strncmp(pp, "..\0", 3)) {
            pp += 2;
            while (bp > base && *--bp != '/')
                ;
        }
        else
            break;
    }

    /*
     * down to leaf.
     */
    length = bp - base;
    if (length >= size)
        goto erange;
    strncpy(result, base, length);
    rp = result + length;
    if (*pp || *(pp - 1) == '/' || length == 0)
        *rp++ = '/';
    if (rp + strlen(pp) > endp)
        goto erange;
    strcpy(rp, pp);

finish:
    return result;

erange:
    errno = ERANGE;
    return (NULL);
}

